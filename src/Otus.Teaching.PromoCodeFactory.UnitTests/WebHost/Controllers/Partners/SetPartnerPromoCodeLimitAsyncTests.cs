﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Collections.Generic;
using System.Xml.Linq;
using System;
using Xunit;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Namotion.Reflection;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        //TODO: Add Unit Tests
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateDefaultPartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("af114767-cc57-4f9b-9fe5-29060739a0ba"),
                Name = "Машины",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("f1476c01-5e4b-433c-916a-a9980cff7b8e"),
                        CreateDate = new DateTime(2023, 4, 1),
                        EndDate = new DateTime(2030, 12, 31),
                        Limit = 10
                    }
                }
            };
            return partner;
        }

        public SetPartnerPromoCodeLimitRequest CreateDefaultLimit()
        {
            var limit = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 10,
                EndDate = new DateTime(2023, 10, 31)
            };
            return limit;
        }

        //1
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("b71dd1d1-69fb-4f0a-a549-3607743a1890");
            Partner partner = null;

            var limit = CreateDefaultLimit();
            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        //2
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("af114767-cc57-4f9b-9fe5-29060739a0ba");
            Partner partner = CreateDefaultPartner();
            partner.IsActive = false;

            var limit = CreateDefaultLimit();
            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        //3
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_ResetNumberIssuedPromoCodes_ReturnsCreatedAtActionResult()
        {
            // Arrange
            var partnerId = Guid.Parse("af114767-cc57-4f9b-9fe5-29060739a0ba");
            Partner partner = CreateDefaultPartner();
            partner.NumberIssuedPromoCodes = 5;

            var limit = CreateDefaultLimit();
            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        //3
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_NotResetNumberIssuedPromoCodes_ReturnsCreatedAtActionResult()
        {
            // Arrange
            var partnerId = Guid.Parse("af114767-cc57-4f9b-9fe5-29060739a0ba");
            Partner partner = CreateDefaultPartner();
            partner.NumberIssuedPromoCodes = 10;

            var limit = CreateDefaultLimit();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(10);
        }

        //4
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_OffCurrentLimit_ReturnsCreatedAtActionResult()
        {
            // Arrange
            var partnerId = Guid.Parse("af114767-cc57-4f9b-9fe5-29060739a0ba");
            Partner partner = CreateDefaultPartner();
            partner.NumberIssuedPromoCodes = 10;

            var limit = CreateDefaultLimit();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            var closeLimit = partner.PartnerLimits
                .Where(x => x.Id == Guid.Parse("f1476c01-5e4b-433c-916a-a9980cff7b8e"))
                .FirstOrDefault().CancelDate.Should().Be(DateTime.Now.Date);
        }

        //5
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_NegativeLimit_ReturnsBadRequestWithValue()
        {
            // Arrange
            var partnerId = Guid.Parse("af114767-cc57-4f9b-9fe5-29060739a0ba");
            Partner partner = CreateDefaultPartner();

            var limit = CreateDefaultLimit();
            limit.Limit = -1;

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result
                .Should().BeAssignableTo<BadRequestObjectResult>()
                .Which.Value.Should().Be("Лимит должен быть больше 0");
        }

        //6
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_CheckNewLimit_ReturnsCreatedAtActionResult()
        {
            // Arrange
            var partnerId = Guid.Parse("af114767-cc57-4f9b-9fe5-29060739a0ba");
            Partner partner = CreateDefaultPartner();

            var limit = CreateDefaultLimit();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            _partnersRepositoryMock
                .Verify(repo => repo
                    .UpdateAsync(It
                        .Is<Partner>(p => p.PartnerLimits
                            .Any(l => l.Limit == limit.Limit && l.EndDate == limit.EndDate))), Times.Once);
        }

    }
}